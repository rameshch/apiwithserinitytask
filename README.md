
## ApiWithSerenityTask
This project is about automating API's using Serenity BDD framework.

## Description
Automating the API's is all about tis project, we can use it as a framework and we can use it in different api projects to test the API before getting into UI level testing. It gives the idea about the application stability from the server side calls so that in the initial stages it self we can fix the service calls to reduce the failures in early stages of the lifecycle.

## Installation & Setup project


#Step 1:
Setting up JAVA:
Install latest Java from the oracle site and set the environment variables.
Please visit the link to download the JAVA - https://www.geeksforgeeks.org/setting-environment-java/ 

#step 2:
Setting up Maven:
Download the Maven from https://maven.apache.org/download.cgi
Please go through the link for the setup - https://maven.apache.org/install.html

#step 3:
Download the IDE:
Just download any IDE like IntelliJ/Eclipse and install in your PC.
For IntelliJ - https://www.jetbrains.com/idea/download/#section=windows
For Eclipse - https://www.eclipse.org/downloads/

#step 4:
Download the GIT tool to clone the project into local repository
Follow the link https://git-scm.com/downloads

#Step 5:
Open git bash
Run git clone command to download the code into local repository
- Git clone https://gitlab.com/rameshch/apiwithserinitytask.git

- It will downlaod all the project from the gitlab cloud to local repo.
- Once it is done, Import the project into IDE prefer IntelliJ and wait until the project build process cmpletes.
- Now you are ready to go 

## Execution
#Through IDE: 

Run through runner class using java as run config:
- Just go to this path src/test/java/starter/  and right click in TestRunner.java file and click on Run. 

Execute Through Maven:
- Open Terminal in IntelliJ
- Run command :  mvn serinity:check   or  use: mvn clean verify

Execution through gitlab pipeline:
- This will be triggered automatically when ever new changes merged into branch.
go through this link for the results page in gitlab - https://gitlab.com/rameshch/apiwithserinitytask/-/pipelines

just used 2 stages but we can configure it based on the needs.


If you want to trigger it manually just in Gitlab need to follow below steps:
before going to execute please request the permission to be a part of the project then only you can able to trigger the Job.
step 1 - open link https://gitlab.com/rameshch/apiwithserinitytask
step 2 - Clink on CICD tag in left pan --> Pipeline --> Click on Run Pipeline.

##Reports
Serenity will generate repots.
You can find the reports in target/site/serenity path.

Screen shots of sample execution reports:
![image.png](./image.png)
![image-1.png](./image-1.png)

## Support
There are wider blogs and videos are available you can go through for any issues.

## Roadmap
Not yet definced

## License
All the tools and softwares used are either open source or community editions.
