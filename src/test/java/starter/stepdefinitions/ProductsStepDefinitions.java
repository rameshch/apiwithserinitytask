package starter.stepdefinitions;

import static net.serenitybdd.rest.SerenityRest.lastResponse;
import static net.serenitybdd.rest.SerenityRest.restAssuredThat;
import static org.hamcrest.Matchers.contains;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ProductsStepDefinitions {
    public static String jsonAsString;

    @Steps
    ProductsAPI productsAPI;

    @When("he calls endpoint with product {word}")
    public void heCallsEndpoint(String product) {
        productsAPI.fetchProductsByName(product);
    }

    @Then("he sees the results displayed for {}")
    public void heSeesTheResultsDisplayedForApple(String product) {
        restAssuredThat(response -> response.statusCode(200));
        jsonAsString = lastResponse().asString();
        assert (jsonAsString.contains(product));
    }

    @Then("he does not see the results for {}")
    public void he_Does_Not_See_The_Results(String product) {
        restAssuredThat(response -> response.statusCode(404));
        jsonAsString = lastResponse().asString();
        assert (jsonAsString.contains("true"));
        //restAssuredThat(response -> response.body("error", contains("True")));
    }
}
