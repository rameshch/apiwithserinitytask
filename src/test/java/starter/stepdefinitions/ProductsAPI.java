package starter.stepdefinitions;

import net.serenitybdd.rest.SerenityRest;
import net.thucydides.core.annotations.Step;

public class ProductsAPI {
    private static final String LIST_BY_PRODUCT_NAME = "https://waarkoop-server.herokuapp.com/api/v1/search/test/{products}";

    @Step("Get product list by name {0}")
    public void fetchProductsByName(String products) {
        SerenityRest.given()
                .pathParam("products", products)
                .get(LIST_BY_PRODUCT_NAME);
    }
}
